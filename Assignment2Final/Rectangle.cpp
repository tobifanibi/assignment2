#include <iostream>
#include <cmath>

using namespace std;
#include "Rectangle.h"
myRectangle::myRectangle(const Point p1,const double recheight, const double recwidth)
    : height(recheight),width(recwidth),UpperLeftVertex(p1)
{
    
    
    
}



myRectangle::myRectangle(Point p1,Point p2)
{
    UpperLeftVertex.setX(min(p1.getX(),p2.getX()));
    UpperLeftVertex.setY(max(p1.getY(),p2.getY()));
    
    LowerRightVertex.setX(max(p1.getX(),p2.getX()));
    LowerRightVertex.setY(min(p1.getY(),p2.getY()));
    
    width=abs(UpperLeftVertex.getX()-LowerRightVertex.getX());
    height=abs(UpperLeftVertex.getY()-LowerRightVertex.getY());
    
    
    
    
    
    
    
}
Point myRectangle::getUpperLeftVertex()
{
    return UpperLeftVertex;
}
double myRectangle::getWidth()
{
    return width;
}

double myRectangle::getHeight()
{
    return height;
}
double myRectangle::getPerimeter()
{
    double perimeter=width*2+height*2;
    return perimeter;
}
Point myRectangle::getCenter()
{
    
    double X=UpperLeftVertex.getX()+(width/2);
    double Y =UpperLeftVertex.getY()-(height/2);
    
    Point Center(X,Y);
    return Center;
}
void myRectangle::translate(double x,double y)
{
    UpperLeftVertex.translate(x,y);
}

bool myRectangle::contains(Point p)
{
    bool contains=false;
    
    Point Upper= getUpperLeftVertex();
    double mywidth = getWidth();
    double myheight = getHeight();
    
    if((Upper.getX()+mywidth)>=p.getX() and Upper.getX()<=p.getX() )
        contains=true;
    if((Upper.getY()-myheight)<=p.getY() and Upper.getX()>=p.getX() )
        contains=true;
    return contains;
    
    
}
double myRectangle::getArea()
{
    double area=width*height;
    return area;
}
